http://rubyonrails.org

rail 围栏

获奖了，理念必有其过人之处
TODO: 你也试试 15 分钟搞个博客站出来

Ruby on Rails其实是一套以Ruby程式语言开发的网页应用程式架构。
而Ruby on Rails是由37 Signals的David Heinemeier Hansson在开发BaseCamp线上专案管理软体时，因为对既有的框架不满意，而为资料库网站开发量身定做出来的架构。

Ruby on Rails於2004年7月时以开放原始码的方式发布，很快的就得到广大的回响。在Windows, Mac, Linux上都能够运作，资料库与Web Server的选择性也很多。

Ruby on Rails的观念在於利用惯例优於组态(convention over configuration)，软体精简(less software)与越快越有生产力(programmer happiness ultimately leads to better productivity)。

Ruby on Rails是完全的Model-View-Control架构，资料库、程式逻辑与呈现完全清楚的分离。在档案名称与目录结构方面，也都定义的很清楚。

资料库这部份(Active Record)是Ruby on Rails最强大的功能，只要设定好资料库连结，之后建立、调整Schema或是其他存取都是直接由Ruby on Rails去控制，不用由我们亲手去动资料库，资料库里的栏位也能和Ruby on Rails里的变数直接对应，不用再像用php的时候，和SQL语法一大堆冒号、引号奋战。另一个可以加快速度的特点在於自动产生使用者界面的程式码(scaffold功能)，这代表我们只要把定义好资料库和要作的动作，Ruby on Rails就会为我们产生使用者界面，我们只要专注在真正要解决的问题上，而不用花太多时间在建立使用者界面上。
 
在测试方面也是Ruby on Rails的强项，它可以为我们自动建立测试档案及资料库，加速测试流程，确保程式品质。
 
不过它能带来的好处，还是要亲自试试看才知道！
 
Ruby on Rail中文网站
http://rubyonrails.org.tw/
 
PS: 官方网站上的影片范例很精彩，15分钟就能开发一个部落格系统、5分钟就写好一个Flickr Mashup。

当然了，所有的 web 框架都有同样的目标。是什么使得 Rails 如此不同呢？我们可以通过几个途径来回答这个问题。
第一是看架构。在过去的时间里，许多开发者在严谨的 web 应用程序上使用 MVC 架构。他们发现 MVC 架构可以使他们的程序变得更清晰，在 java 中像 Tapestry 和 Struts 这些框架都是基于 MVC 。 Rails 也是一个

MVC 框架。当你使用 Rails进行开发，应用程序的所有代码以一种标准方式互相作用。在开始开发之前，整个应用程序的骨架已经搭好的。
回答这个问题的第二个是看编程语言。 Rails 程序是使用 Ruby 编写的，它是一门现代，面向对象的脚本语言。 Ruby 简洁，不难理解。它可以让你快速地用代码自然 , 清晰表达想法。让你的程序能很简单被编写并且在几个月后还能很容易读懂。
Ruby on Rails是一个 Web 应用程序框架,是一个相对较新的 Web 应用程序框架，构建在 Ruby 语言之上。它被宣传为现有企业框架的一个替代，而它的目标，简而言之，就是让生活，至少是 Web 开发方面的生活，变得更轻松。